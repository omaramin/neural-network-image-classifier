# Omar Amin
# Dr. Metzler
# CMSC426
# 04/04/2021

import numpy as np
import time
import matplotlib.pyplot as plt

train_images_np=np.load('./Project3_Data/MNIST_train_images.npy')
train_labels_np=np.load('./Project3_Data/MNIST_train_labels.npy')
val_images_np=np.load('./Project3_Data/MNIST_val_images.npy')
val_labels_np=np.load('./Project3_Data/MNIST_val_labels.npy')
test_images_np=np.load('./Project3_Data/MNIST_test_images.npy')
test_labels_np=np.load('./Project3_Data/MNIST_test_labels.npy')

##Template MLP code
def softmax(x):
    return np.exp(x)/np.sum(np.exp(x))

def sigmoid(x):
    return 1/(1+np.exp(-x))

def CrossEntropy(y_hat,y):
    return -np.dot(y,np.log(y_hat))

# Adapted from: TA office hours code
def derivSig(x):
    return sigmoid(x)*(1 - sigmoid(x))

class MLP():

    # 2.2
    def __init__(self):
        #Initialize all the parameters
        self.W1 = np.random.normal(scale = 0.1, size = (64, 784))
        self.b1 = np.zeros((64, 1))
        self.W2 = np.random.normal(scale = 0.1, size = (10, 64))
        self.b2 = np.zeros((10, 1))
        self.reset_grad()

    def reset_grad(self):
        self.W2_grad = 0
        self.b2_grad = 0
        self.W1_grad = 0
        self.b1_grad = 0

    # 2.1
    def forward(self, x):
        #Feed data through the network
        self.x = np.transpose(x[None])
        self.W1x = np.dot(self.W1, self.x)
        self.a1 = np.add(self.W1x, self.b1)
        self.f1 = sigmoid(self.a1)
        self.W2x = np.dot(self.W2, self.f1)
        self.a2 = np.add(self.W2x, self.b2)
        self.y_hat = softmax(self.a2)
        return self.y_hat

    # 2.3.2
    def update_grad(self,y):
        # Compute the gradients for the current observation y and add it to the gradient estimate over the entire batch
        expect = np.zeros(shape = (10, 1))
        expect[y] = 1

        dA2db2 = 1
        dA2dW2 = self.f1
        dA2dF1 = self.W2
        dF1dA1 = derivSig(self.a1)
        dA1db1 = 1
        dA1dW1 = self.x
        dLdA2 = (derivSig(self.a2))*(self.y_hat - expect)

        dA2dW2_t = np.transpose(dA2dW2) # dA2dW2^T
        dLdW2 = np.dot(dLdA2, dA2dW2_t) 
        
        dLdb2 = np.dot(dLdA2, dA2db2) # dA2db2 = 1

        dLdA2_t = np.transpose(dLdA2)
        dLdF1 = np.dot(dLdA2_t, dA2dF1) # dA2dF1 = w2

        dF1dA1_t = np.transpose(dF1dA1)
        dLdA1 = np.multiply(dLdF1, dF1dA1_t) # dF1dA1^T

        dLdA1_t = np.transpose(dLdA1)
        dLdW1 = np.dot(dLdA1_t, dA1dW1.reshape(1,784))

        dLdb1 = np.dot(dLdA1, dA1db1) # dA1db1 = 1

        self.W2_grad = self.W2_grad + dLdW2
        self.b2_grad = self.b2_grad + dLdb2
        self.W1_grad = self.W1_grad + dLdW1
        self.b1_grad = self.b1_grad + dLdb1

    def update_params(self,learning_rate):
        self.W2 = self.W2 - learning_rate * self.W2_grad
        self.b2 = self.b2 - learning_rate * self.b2_grad
        self.W1 = self.W1 - learning_rate * self.W1_grad
        self.b1 = self.b1 - learning_rate * np.transpose(self.b1_grad)

# Init the MLP
myNet=MLP()

# Load weights/biases (Adapted from: https://machinelearningmastery.com/how-to-save-a-numpy-array-to-file-for-machine-learning/)
myNet.W1 = np.load('./Amin_Omar_MLP_W1.npy')
myNet.W2 = np.load('./Amin_Omar_MLP_W2.npy')
myNet.b1 = np.load('./Amin_Omar_MLP_b1.npy')
myNet.b2 = np.load('./Amin_Omar_MLP_b2.npy')

learning_rate = 1e-3
n_epochs = 100

# 2.4: Training code
training_accuracy = []
validation_accuracy = []

# Compare actual vs. expected values and extract correct percentage
def validation(train_images_np, train_labels_np):
    count_accuracy = 0
    actual_values = []
    percentage = 0

    for l in range(len(train_images_np)):
        max_index = 0
        max = -1
        results = myNet.forward(train_images_np[l])
        for m in range(len(results)):
            if results[m] > max:
                max = results[m]
                max_index = m
        actual_values.append(max_index)

    label_index = 0
    for n in actual_values:
        if train_labels_np[label_index] == n:
            count_accuracy = count_accuracy + 1
        label_index += 1

    percentage = (count_accuracy/len(actual_values))*100
    return percentage, actual_values

def training(n_epochs, learning_rate, train_images_np, train_labels_np, val_images_np, val_labels_np):
    row, col = train_images_np.shape
    even_batches = int(row/256)

    training_accuracy.clear()
    validation_accuracy.clear()
    for iter in range(n_epochs):
        batch_num = 0
        for j in range(0, row, 256):
            myNet.reset_grad()
            start = batch_num*256
            end = j + 256
            # Handle leftover images
            if batch_num > (even_batches - 1):
                curr_batch = train_images_np[start : row]
                curr_labels = train_labels_np[start : row]
            # Normal batch operation
            else:
                curr_batch = train_images_np[start : end]
                curr_labels = train_labels_np[start : end]
            for k in range(len(curr_batch)):
                myNet.forward(curr_batch[k])
                myNet.update_grad(curr_labels[k])
            
            myNet.update_params(learning_rate)
            # Track sets of 256 images
            batch_num += 1
        # Compute validation loss/accuracy
        t_accuracy, t_actuals = validation(train_images_np, train_labels_np)
        v_accuracy, v_actuals = validation(val_images_np, val_labels_np)
        training_accuracy.append(t_accuracy)
        validation_accuracy.append(v_accuracy)
    # Plot accuracy/epoch
    plt.title(str(row) + " Image Set")
    plt.axis([0, 100, 75, 100])
    plt.plot(training_accuracy, color = 'r', label = 'Training')
    plt.plot(validation_accuracy, color = 'b', label = 'Validation')
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy(%)")
    plt.legend(loc = "lower right")
    plt.show()

# 2.5: Test on first 2000 images
"""training(n_epochs, learning_rate, train_images_np[:2000], train_labels_np[:2000], val_images_np, val_labels_np)
print("2000 Image Set Latest Training Accuracy: " + str(max(training_accuracy)))
print("2000 Image Set Latest Validation Accuracy: " + str(max(validation_accuracy)))

# Reset the training
myNet.__init__()

# Test on full training set
training(n_epochs, learning_rate, train_images_np, train_labels_np, val_images_np, val_labels_np)
print("50000 Image Set Latest Training Accuracy: " + str(max(training_accuracy)))
print("50000 Image Set Latest Validation Accuracy: " + str(max(validation_accuracy)))"""

# 2.6: Apply to testing data set
acc, actual = validation(test_images_np, test_labels_np)
print("Testing data set Accuracy: " + str(acc))

# 2.7: Confusion Matrix
def helper(actual, res, i, row):
    if actual[i] == 0:
        res[row][0] += 1
    elif actual[i] == 1:
        res[row][1] += 1
    elif actual[i] == 2:
        res[row][2] += 1
    elif actual[i] == 3:
        res[row][3] += 1
    elif actual[i] == 4:
        res[row][4] += 1
    elif actual[i] == 5:
        res[row][5] += 1
    elif actual[i] == 6:
        res[row][6] += 1
    elif actual[i] == 7:
        res[row][7] += 1
    elif actual[i] == 8:
        res[row][8] += 1
    elif actual[i] == 9:
        res[row][9] += 1

res = np.zeros(shape = (10, 10))

for i in range(len(test_labels_np)):
    if test_labels_np[i] == 0:
        helper(actual, res, i, 0)
    elif test_labels_np[i] == 1:
        helper(actual, res, i, 1)
    elif test_labels_np[i] == 2:
        helper(actual, res, i, 2)
    elif test_labels_np[i] == 3:
        helper(actual, res, i, 3)
    elif test_labels_np[i] == 4:
        helper(actual, res, i, 4)
    elif test_labels_np[i] == 5:
        helper(actual, res, i, 5)
    elif test_labels_np[i] == 6:
        helper(actual, res, i, 6)
    elif test_labels_np[i] == 7:
        helper(actual, res, i, 7)
    elif test_labels_np[i] == 8:
        helper(actual, res, i, 8)
    elif test_labels_np[i] == 9:
        helper(actual, res, i, 9)

# Convert to percentages
for i in range(len(res)):
    sum = np.sum(res[i])
    for j in range(len(res[i])):
        res[i][j] = (res[i][j] / sum) * 100
        res[i][j] = np.format_float_positional(res[i][j], precision = 2)

# Display confusion matrix
print(res)

# 2.8: Visualize rows of W1
all_img = plt.figure()
for i in range(len(myNet.W1)):
    img = all_img.add_subplot(8, 8, i + 1)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(np.reshape(myNet.W1[i], (28, 28)))
plt.show()

# Save weights/biases (Adapted from: https://machinelearningmastery.com/how-to-save-a-numpy-array-to-file-for-machine-learning/)
"""np.save('./Project3_Data/Amin_Omar_MLP_W1.npy', myNet.W1)
np.save('./Project3_Data/Amin_Omar_MLP_W2.npy', myNet.W2)
np.save('./Project3_Data/Amin_Omar_MLP_b1.npy', myNet.b1)
np.save('./Project3_Data/Amin_Omar_MLP_b2.npy', myNet.b2)"""

## Template for ConvNet Code
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

train_images_np_cnn = np.load('./Project3_Data/MNIST_train_images.npy')
train_labels_np_cnn = np.load('./Project3_Data/MNIST_train_labels.npy')
val_images_np_cnn = np.load('./Project3_Data/MNIST_val_images.npy')
val_labels_np_cnn = np.load('./Project3_Data/MNIST_val_labels.npy')
test_images_np_cnn = np.load('./Project3_Data/MNIST_test_images.npy')
test_labels_np_cnn = np.load('./Project3_Data/MNIST_test_labels.npy')

class ConvNet(nn.Module):
    # From https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html
    def __init__(self):
        super(ConvNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 4 * 4, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x.view(-1,1,28,28))))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 4 * 4)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

# Basic setup adapted from the tutorial
net = ConvNet()
PATH = "./Amin_Omar_Pytorch_state_dict_model.pt"

# Load previously trained weights (Adapted from: https://pytorch.org/tutorials/recipes/recipes/saving_and_loading_models_for_inference.html)
net.load_state_dict(torch.load(PATH))
net.eval()

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr = 0.001, momentum = 0.9)

# Hold percentages for plotting
training_accuracy_CNN = []
validation_accuracy_CNN = []

# Compare actual vs. expected values and extract correct percentage
def validation_CNN(train_images_np, train_labels_np):
    row, col = train_images_np.shape
    even_batches = int(row/256)

    correct = 0
    total = 0
    with torch.no_grad():
        batch_num = 0
        for l in range(0, row, 256):
            start = batch_num*256
            end = l + 256

            # Handle leftover images
            if batch_num > (even_batches - 1):
                curr_batch = train_images_np[start : row]
                curr_labels = train_labels_np[start : row]
            # Normal batch operation
            else:
                curr_batch = train_images_np[start : end]
                curr_labels = train_labels_np[start : end]
            batch_num += 1

            # Make each image in batch a tensor
            for img in curr_batch:
                img = torch.from_numpy(curr_batch).float()

            # Turn entire batch/labels into a tensor
            curr_batch = torch.from_numpy(curr_batch).float()
            curr_labels = torch.from_numpy(curr_labels)
            curr_labels = curr_labels.type(torch.LongTensor)

            # New pytorch method from tutorial
            outputs = net(curr_batch)

            _, predicted = torch.max(outputs.data, 1)
            total += curr_labels.size(0)
            correct += (predicted == curr_labels).sum().item()

        percentage = 100 * correct / total
    return percentage

def training_CNN(n_epochs, learning_rate, train_images_np, train_labels_np, val_images_np, val_labels_np):
    row, col = train_images_np.shape
    even_batches = int(row/256)

    training_accuracy_CNN.clear()
    validation_accuracy_CNN.clear()
    for iter in range(n_epochs):
        batch_num = 0
        for j in range(0, row, 256):
            # New pytorch reset from tutorial
            optimizer.zero_grad()

            start = batch_num*256
            end = j + 256
            # Handle leftover images
            if batch_num > (even_batches - 1):
                curr_batch = train_images_np[start : row]
                curr_labels = train_labels_np[start : row]
            # Normal batch operation
            else:
                curr_batch = train_images_np[start : end]
                curr_labels = train_labels_np[start : end]
            
            # Make each image in batch a tensor
            for img in curr_batch:
                img = torch.from_numpy(curr_batch).float()

            # Turn entire batch/labels into a tensor
            curr_batch = torch.from_numpy(curr_batch).float()
            curr_labels = torch.from_numpy(curr_labels)
            curr_labels = curr_labels.type(torch.LongTensor)

            # New pytorch method from tutorial
            outputs = net(curr_batch)
            loss = criterion(outputs, curr_labels)
            loss.backward()
            optimizer.step()
            
            # Track sets of 256 images
            batch_num += 1
        # Compute validation loss/accuracy
        training_accuracy_CNN.append(validation_CNN(train_images_np, train_labels_np))
        validation_accuracy_CNN.append(validation_CNN(val_images_np, val_labels_np))
    # Plot accuracy/epoch
    plt.title(str(row) + " Image Set")
    plt.axis([0, 100, 75, 100])
    plt.plot(training_accuracy_CNN, color = 'r', label = 'CNN Training')
    plt.plot(validation_accuracy_CNN, color = 'b', label = 'CNN Validation')
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy(%)")
    plt.legend(loc = "lower right")
    plt.show()

# 3.3
"""training_CNN(n_epochs, learning_rate, train_images_np_cnn[:2000], train_labels_np_cnn[:2000], val_images_np_cnn, val_labels_np_cnn)
print("2000 Image Set CNN Latest Training Accuracy: " + str(max(training_accuracy_CNN)))
print("2000 Image Set CNN Latest Validation Accuracy: " + str(max(validation_accuracy_CNN)))

# Test on full training set
training_CNN(n_epochs, learning_rate, train_images_np_cnn, train_labels_np_cnn, val_images_np_cnn, val_labels_np_cnn)
print("50000 Image Set CNN Latest Training Accuracy: " + str(max(training_accuracy_CNN)))
print("50000 Image Set CNN Latest Validation Accuracy: " + str(max(validation_accuracy_CNN)))"""

# 3.4: Apply to testing data set
acc = validation_CNN(test_images_np_cnn, test_labels_np_cnn)
print("Testing data set CNN Accuracy: " + str(acc))

# Save Pytorch weights/data (Adapted from: https://pytorch.org/tutorials/recipes/recipes/saving_and_loading_models_for_inference.html)
# torch.save(net.state_dict(), PATH)